# growingSTEMS Conventions
The following is a listing of all documented conventions used by growingSTEMS. A majority of the conventions used are simply standards that a large amount of people use.
Each convention section in this file will contain the following:
* links to any relevant convention documentation
* section detailing changes that were made to the standard at the provided link
* section detailing any assumptions/interpretations made about information provided at the link

## Coding Conventions (Style Guides)
### Relevant Links
| Language | Style Guide Link |
| ------ | ------ |
| Java | https://growingstems.gitlab.io/styleguide/javaguide.html |

### Notes
The langauges listed in the table above are the languages growingSTEMS uses enough to warrant taking the time to fully document a style guide.
It is not an exhaustive list of all languages used by growingSTEMS. The style guides created by growingSTEMS are most often modified versions of style guides created
by Google.
* If using a language that does not appear in the table, default to the style guides found at https://google.github.io/styleguide/

## Units/Coordinate Spaces
### Relevant Links
https://www.ros.org/reps/rep-0103.html

### Key Differences
The default unit for distance should be inches.

### Additional Unit Conventions
When angles are normalized, they shall be normalized to the range (-pi, pi]. If an normalization would result in a 180 degree angle, that angle shall be positive.

### Notes
For field central coordinate space, the following are true:
* positive-X is downfield away from the driver
* positive-Y is to the left
* positive-Z is up
